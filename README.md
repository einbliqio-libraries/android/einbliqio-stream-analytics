# EINBLIQ.IO stream analytics library

The `einbliqio-stream-analytics` library measures the quality of service for video and audio playback sessions.

In order to collect quality of service playback analytics data, the `einbliqio-stream-analytics` library needs to be integrated into the android application. This is a three step process:

1. Adding `einbliqio-stream-analytics` dependency
2. Coupling with `ExoPlayer`
3. Adding `ExoPlayer` Listener

After the integration process, the library will send analytics data to the EINBLIQ.IO servers for each playback session of a video or audio stream.

## 1. Adding einbliqio-stream-analytics dependency 

The library can be integrated as a maven dependency. Therefore, add to your `build.gradle` file: 

```gradle
allprojects {
    repositories {
        [...] // google, jitpack, etc.
        maven {
            url "https://gitlab.com/api/v4/projects/26569572/packages/maven"
            name "GitLab"
            credentials(HttpHeaderCredentials) {
                name = "Deploy-Token"
                value = "<einbliqIoDeployToken>" // will be provided by team@einbliq.io
            }
            authentication {
                header(HttpHeaderAuthentication)
            }
        }

    }
}
```

> Note: The `einbliqIoDeployToken` value will be provided by `team@einbliq.io`.


And in your module `build.gradle` add:

```gradle
dependencies {
    implementation "io.einbliq:streamanalytics:1.3.0"
}
```


## 2. Coupling with ExoPlayer

In order to collect playback analytics data, the library needs to be coupled with the `ExoPlayer` within the application.
The coupling is an easy three step process:

### 2.1. Creating EinbliqIoSession

First, an `EinbliqIoSession` needs to be created within your media player activity. Each Session needs to be initialised with an  [EinbliqIoSessionConfiguration](#EinbliqIo.SessionConfiguration) object:

```kotlin
// importing dependencies
import io.einbliq.streamanalytics.EinbliqIoSession;
import io.einbliq.streamanalytics.EinbliqIoSessionConfiguration;

// creating EINBLIQ.IO super session
// Only needed, if multiple player instances are used for ad-insertion!
// See: 2.1.1. Handling Ad-Insertion
// EinbliqIoSession.createSuperSession();

// creating session configuration        
val configuration = EinbliqIoSessionConfiguration.Builder(
    customerId, // Mandatory: customerId value will be provided by team@einbliq.io
    "https://myserver.com/myvideo.m3u8") // Mandatory: videoUrl
    .withMediaId("123456") // Recommended, if present
    .withMediaTitle("myMediaTitle") // Recommended
    .withMediaSeries("myMediaSeries") // Recommended
    .withMediaCategory("myMediaCategory") // Recommended
    .withApplicationName("myApplication") // Recommended
    .withApplicationVersion("v2.6.3") // Reccomended
    .withPowerTrackingEnabled(context) // Recommended
    // .withLoggingEnabled(BuildConfig.DEBUG)
    .build();

// creating EinbliqIoSession  
private EinbliqIoSession einbliqIoSession = new EinbliqIoSession(configuration);
```

Each `EinbliqIoSession` collects the respective data and sends it to the `EINBLIQ.IO` backend autonomously.

> Note 1: For each playback session a separate `EinbliqIoSession` needs to be created. If you reuse the `ExoPlayer` to play back multiple media assets, see [Case: Reusing existing ExoPlayer instance for playing multiple media assets](#reusingexoplayer).

#### 2.1.1. Handling Ad-Insertion

There are two ways to add advertisements in an application:

- client-side-ad-insertion
- server-side-ad-insertion

Both are handled by the `einbliqio-stream-analytics` library autonomously. 

**Inserting Ads with multiple player instances**

However, if multiple player instances are being used for ad-insertion (for example one player instance for advertisements and one for content), a `SuperSession` needs to be created first, by calling:

```kotlin
// creating EINBLIQ.IO super session
EinbliqIoSession.createSuperSession();

// creating EinbliqIoSession  
private EinbliqIoSession einbliqIoSession = new EinbliqIoSession(configuration);
``` 

This `SuperSession` links the data of multiple player instances. 


### 2.2. Integrating EinbliqIoSession into App Lifecycle

After creation, the `EinbliqIoSession` needs to listen to specific app lifecycle events in order to measure media consumption correctly.

```kotlin
override fun onResume() {
    super.onResume()
    [...]
    einbliqIoSession?.resume()
}

override fun onPause() {
    super.onPause()
    [...]
    einbliqIoSession?.pause()
}

override fun onDestroy() {
    super.onDestroy()
    [...]
    einbliqIoSession?.terminate()
    einbliqIoSession = null
}
```

### 2.3. Adding ExoPlayer Listener

Lastly, the `EinbliqIoSession` needs to be coupled with the `ExoPlayer`. 

In order to ease the integration process, the 
`einbliqio-stream-analytics` library provides `EinbliqIoExoListener`s that take care of the coupling between the `ExoPlayer` and the `EinbliqIoSession`. 

Therefore, after creating an `ExoPlayer` object, the `EinbliqIoExoListener` needs to be added as a new `AnalyticsListener`:

```kotlin
// adding EinbliqIoExoListener to ExoPlayer that handles coupling between session and ExoPlayer
exoPlayer.addAnalyticsListener(EinbliqIoExoListener(einbliqIoSession))
```

> Note: Specific `EinbliqIoExoListener` for all recent `ExoPlayer` versions are in the [src](./src) folder of this repository.

The `EinbliqIoExoListener` handles the coupling and data exchange between the `ExoPlayer` and the `EinbliqIoSession`. 

No further integration or coupling is needed.

<a name="reusingexoplayer"></a>

### Case: Reusing existing ExoPlayer instance for playing multiple media assets

In case the `ExoPlayer` is reused and plays back multiple media assets, make sure to:

- terminate the previous `EinbliqIoSession` when a new media asset is loaded
```kotlin
// terminating previous session
einbliqIoSession?.terminate()
```
- remove the previous `EinbliqIoExoListener`
- create a new `EinbliqIoSession`
- create and add a new `EinbliqIoExoListener`


<a name="EinbliqIo.SessionConfiguration"></a>

## SessionConfiguration

The `SessionConfiguration` interface defines the metadata that can be passed on to an `EinbliqIoSession`.

| Param | Type | Description |
| --- | --- | --- |
| customerId | <code>String</code> | customerId. Provided by team@einbliq.io |
| mediaUrl | <code>String</code> | mediaUrl |
| [mediaId] | <code>String</code> | Recommended. Identifier of media asset |
| [mediaTitle] | <code>String</code> | Recommended: Title of media asset |
| [mediaSeries] | <code>String</code> | Recommended: Series of media asset |
| [mediaCategory] | <code>String</code> | Recommended: Category of media asset |
| [termId] | <code>String</code> | Recommended: termId of media asset |
| [crid] | <code>String</code> | Recommended: CRID of media asset |
| [applicationName] | <code>String</code> | Recommended: Application Name |
| [applicationVersion] | <code>String</code> | Recommended: Application Version |
| [customData] | <code>Hashtable<String, Object></code> | Optional: Custom Data. Can be any additional data that should be tracked alongside with playback metrics. All `objects` should be primitive values that can be parsed to JSON. |
| [autoPreload] | <code>Boolean</code> | Optional: Auto Preload Flag. Should be set to `true` if player auto preloads content before playback. |
| [loggingEnabled] | <code>boolean</code> | Optional: Logging Enabled Flag. Should be set tot `true` during development process to check if data is collected correctly |
| [powerTrackingEnabled] | <code>Context</code> | Recommended: Enables power tracking (battery details, screen brightness, power consumption), requires a `Context` object (the `ApplicationContext` is stored internally to avoid memory leaks) |

**Example**  
```kotlin
// creating session configuration        
val configuration = EinbliqIoSessionConfiguration.Builder(
    customerId, // Mandatory: customerId value will be provided by team@einbliq.io
    "https://myserver.com/myvideo.m3u8") // Mandatory: videoUrl
    .withMediaId("123456") // Recommended, if present
    .withMediaTitle("myMediaTitle") // Recommended
    .withMediaSeries("myMediaSeries") // Recommended
    .withMediaCategory("myMediaCategory") // Recommended
    .withTermId("myTermId") // Recommended, if present
    .withCrid("myCRID") // Recommended, if present
    .withApplicationName("myApplication") // Recommended
    .withApplicationVersion("v2.6.3") // Recommended
    .withPowerTrackingEnabled(context) // Recommended
    // .withAutoPreload(true)
    // .withLoggingEnabled(BuildConfig.DEBUG)
    .build();
```

## Testing Integration

With `loggingEnabled` set to `true` within the `EinbliqIoSessionConfiguration`, you see all information that is sent by the `EinbliqIoSession` as raw json data in logcat with loglevel `DEBUG` under the tag `einbliqIoLogger`.

Each JSON object should contain the current session id (`sid`) and request number (`rn`).

Verify: 

- [ ] General Integration: On playback start, a JSON object is sent containing all information passed to the configuration object.
- [ ] General Integration: On playback end, no more data is sent.
- [ ] Lifecycle integration: Verifying that a session is surviving orientation change
- [ ] Lifecycle integration: On activity pause, all current data is sent immediately and the EinbliqIoSession is paused. No further data is sent until playback is resumed. 
- [ ] Lifecycle integration: On activity resume, the session is resumed and starts sending data again as before.


## CHANGELOG

v1.3.0 (2025-03-05)
- *NEW* @OptIn Annotation for EinbliqIoListener 
- *FIX* CMCD header parsing fixed

v1.2.0 (2023-06-29)
- *NEW* CMCD support (req. androidx.media3 1.1.0+)
- *NEW* DRM support
- *NEW* NetworkThroughput support
- *NEW* support for Periods/Super-Session/Ads
- *NEW* tracking of more non-fatal errors
- *NEW* decoder initialization details 
- *NEW* add EinbliqIoListener implementation for Exoplayer 2.15-2.17 
- *NEW* deprecate Exoplayer versions lower than 2.15 (contact us, if you cannot update) 
- *CHG* exoplayer version is tracked in EinbliqIoListener 
- *FIX* minor lifecycle improvements

v1.1.0 (2022-12-22)
- *NEW* Power Tracking (requires configuration change! Add `.withPowerTrackingEnabled(context)` to your configuration) 
- *FIX* minor lifecycle improvements

v1.0.0 (2021-08-01)
- Initial release of the EINBLIQ.IO stream analytics Android library

