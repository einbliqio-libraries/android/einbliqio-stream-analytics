package io.einbliq.streamanalytics.adapter

import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.ExoPlayerLibraryInfo
import com.google.android.exoplayer2.analytics.AnalyticsListener
import com.google.android.exoplayer2.source.LoadEventInfo
import com.google.android.exoplayer2.source.MediaLoadData
import com.google.android.exoplayer2.util.MimeTypes
import io.einbliq.streamanalytics.CmcdModel
import io.einbliq.streamanalytics.DrmModel
import io.einbliq.streamanalytics.EinbliqIoExoPlayerMappings.mapExoIsPlayingToEinbliqPlayState
import io.einbliq.streamanalytics.EinbliqIoExoPlayerMappings.mapExoPlayStateToEinbliqPlayState
import io.einbliq.streamanalytics.EinbliqIoSession
import io.einbliq.streamanalytics.ErrorSeverities
import io.einbliq.streamanalytics.PeriodModel
import io.einbliq.streamanalytics.PlayStates
import java.io.IOException

// EinbliqIoExoListener for ExoPlayer 2.18.x - 2.19.x
@Suppress("ClassName")
class EinbliqIoExoListener2_18(private val einbliqIoSession: EinbliqIoSession) : AnalyticsListener {

    private var drmConfiguration: MediaItem.DrmConfiguration? = null
    private var isPeriodSet = false
    override fun onEvents(player: Player, events: AnalyticsListener.Events) {
        super.onEvents(player, events)
        player.apply {
            if (!isPeriodSet) {
                isPeriodSet = true
                setExoplayerInformation()
                val period = currentTimeline.getPeriod(currentPeriodIndex, Timeline.Period())
                val isAd = currentAdGroupIndex != -1
                val periodModel = PeriodModel(
                    periodNumber = currentPeriodIndex,
                    periodUri = currentMediaItem?.localConfiguration?.uri?.toString(),
                    serverSideAd = if (isAd)period.isServerSideInsertedAdGroup(currentAdGroupIndex) else null,
                    adGroup = if (isAd) currentAdGroupIndex else null,
                    adIdInGroup = if (isAd) currentAdIndexInAdGroup else null,
                    durationMs = if (period.durationMs < 0L) null else period.durationMs,
                    oldPosition = null,
                    newPosition = currentPosition
                )
                einbliqIoSession.addPeriodInformation(periodModel)
            }
            if (currentMediaItem?.localConfiguration?.drmConfiguration == drmConfiguration) return
            drmConfiguration = currentMediaItem?.localConfiguration?.drmConfiguration
            drmConfiguration?.toDrmModel()?.let { einbliqIoSession.addDrmInformation(it) }
        }
    }

    private fun setExoplayerInformation() {
        val customData = mapOf(
            "android.player" to "exoplayer",
            "android.player.version" to ExoPlayerLibraryInfo.VERSION
        )
        einbliqIoSession.addCustomData(customData)
    }

    override fun onPlaybackStateChanged(eventTime: AnalyticsListener.EventTime, state: Int) {
        val einbliqPlayState = mapExoPlayStateToEinbliqPlayState(state)
        einbliqIoSession.addPlayState(einbliqPlayState, eventTime.eventPlaybackPositionMs, null)
        if (einbliqPlayState == PlayStates.ENDED)
            einbliqIoSession.terminate()
    }

    override fun onLoadStarted(
        eventTime: AnalyticsListener.EventTime,
        loadEventInfo: LoadEventInfo,
        mediaLoadData: MediaLoadData
    ) {
        super.onLoadStarted(eventTime, loadEventInfo, mediaLoadData)
        val reqHeader = loadEventInfo.dataSpec.httpRequestHeaders
        val cmcd = CmcdModel.parseCmcdHeader(reqHeader)
        if (cmcd.isNotEmpty())
            einbliqIoSession.addCmcdInformation(cmcd)
    }

    override fun onLoadCompleted(
        eventTime: AnalyticsListener.EventTime,
        loadEventInfo: LoadEventInfo,
        mediaLoadData: MediaLoadData
    ) {
        if (mediaLoadData.trackType != C.TRACK_TYPE_DEFAULT &&
            mediaLoadData.trackType != C.TRACK_TYPE_VIDEO
        ) return
        val durationMs = loadEventInfo.loadDurationMs
        if (durationMs < 25) return
        val throughput = loadEventInfo.bytesLoaded
        if (throughput < 2_000L) return
        einbliqIoSession.addCurrentNetworkThroughput((throughput * 8 / durationMs).toInt())
    }

    override fun onIsPlayingChanged(eventTime: AnalyticsListener.EventTime, isPlaying: Boolean) {
        val state = mapExoIsPlayingToEinbliqPlayState(isPlaying)
        einbliqIoSession.addPlayState(state, eventTime.eventPlaybackPositionMs, null)
    }

    override fun onPositionDiscontinuity(
        eventTime: AnalyticsListener.EventTime,
        oldPosition: Player.PositionInfo,
        newPosition: Player.PositionInfo,
        reason: Int
    ) {
        if (!oldPosition.isSameEinbliqSessionPeriodAs(newPosition)) {
            val period = eventTime.timeline.getPeriod(newPosition.periodIndex, Timeline.Period())
            val periodModel = getPeriodModel(oldPosition, newPosition, period)
            einbliqIoSession.addPeriodInformation(periodModel)
        }
        val playState = when (reason) {
            Player.DISCONTINUITY_REASON_AUTO_TRANSITION -> PlayStates.DISCONTINUITY_REASON_AUTO_TRANSITION
            Player.DISCONTINUITY_REASON_SEEK -> PlayStates.SEEKING
            Player.DISCONTINUITY_REASON_SEEK_ADJUSTMENT -> PlayStates.DISCONTINUITY_REASON_SEEK_ADJUSTMENT
            Player.DISCONTINUITY_REASON_SKIP -> PlayStates.DISCONTINUITY_REASON_SKIP
            Player.DISCONTINUITY_REASON_REMOVE -> PlayStates.DISCONTINUITY_REASON_REMOVE
            Player.DISCONTINUITY_REASON_INTERNAL -> PlayStates.DISCONTINUITY_REASON_INTERNAL
            
            else -> return
        }
        einbliqIoSession.addPlayState(playState, eventTime.eventPlaybackPositionMs, null)
    }

    private fun getPeriodModel(
        oldPosition: Player.PositionInfo,
        newPosition: Player.PositionInfo,
        period: Timeline.Period
    ): PeriodModel {
        val isAd = newPosition.adGroupIndex != -1
        return PeriodModel(
            periodNumber = newPosition.periodIndex,
            periodUri = newPosition.mediaItem?.localConfiguration?.uri?.toString(),
            serverSideAd = if (isAd) period.isServerSideInsertedAdGroup(newPosition.adGroupIndex) else null,
            adGroup = if (isAd) newPosition.adGroupIndex else null,
            adIdInGroup = if (isAd) newPosition.adIndexInAdGroup else null,
            durationMs = period.durationMs,
            oldPosition = oldPosition.positionMs,
            newPosition = newPosition.positionMs
        )
    }

    private fun Player.PositionInfo.isSameEinbliqSessionPeriodAs(other: Player.PositionInfo): Boolean =
        periodIndex == periodIndex &&
                adGroupIndex == other.adGroupIndex &&
                adIndexInAdGroup == other.adIndexInAdGroup &&
                mediaItem?.localConfiguration?.uri == other.mediaItem?.localConfiguration?.uri

    override fun onPlaybackParametersChanged(
        eventTime: AnalyticsListener.EventTime,
        playbackParameters: PlaybackParameters
    ) {
        einbliqIoSession.addCustomData(
            hashMapOf("android.playbackSpeed" to playbackParameters.speed)
        )
    }

    override fun onPlayerError(eventTime: AnalyticsListener.EventTime, error: PlaybackException) {
        einbliqIoSession.addError(
            error.message ?: "unknown exoplayer error",
            ErrorSeverities.ERROR,
            error.cause?.message
        )
    }

    override fun onLoadError(
        eventTime: AnalyticsListener.EventTime,
        loadEventInfo: LoadEventInfo,
        mediaLoadData: MediaLoadData,
        error: IOException,
        wasCanceled: Boolean
    ) {
        einbliqIoSession.addError(
            error.message ?: "unknown exoplayer error",
            ErrorSeverities.WARNING,
            error.cause?.message
        )
    }

    override fun onTracksChanged(eventTime: AnalyticsListener.EventTime, tracks: Tracks) {
        // verify format support
        if (tracks.containsType(C.TRACK_TYPE_VIDEO) && !tracks.isTypeSupported(C.TRACK_TYPE_VIDEO, true))
            einbliqIoSession.addError("No supported video track found", ErrorSeverities.CRITICAL)
        if (tracks.containsType(C.TRACK_TYPE_AUDIO) && !tracks.isTypeSupported(C.TRACK_TYPE_AUDIO, true))
            einbliqIoSession.addError("No supported audio track found", ErrorSeverities.CRITICAL)

        // this is for switching audio track, subtitle track or video quality
        var textTrackFormat: Format? = null
        var activeTextTrackFound = false
        tracks.groups
            .filterNot { it.length == 0 }
            .forEach { group ->
                val firstSelectedIndex = 0.until(group.length).firstOrNull { group.isTrackSelected(it) }
                val format = group.getTrackFormat(firstSelectedIndex ?: 0)
                val trackType = format.getPrimaryTrackType()
                if (textTrackFormat == null && trackType == C.TRACK_TYPE_TEXT) textTrackFormat = format
                if (firstSelectedIndex == null) return@forEach
                when (trackType) {
                    C.TRACK_TYPE_TEXT -> {
                        einbliqIoSession.addCustomData(format.toTextTrackCustomData(true))
                        activeTextTrackFound = true
                    }

                    C.TRACK_TYPE_AUDIO -> {
                        einbliqIoSession.addCustomData(format.toAudioTrackCustomData())
                    }

                    C.TRACK_TYPE_VIDEO -> {
                        val bitrateKbps = format.bitrate / 1000
                        einbliqIoSession.addPlayState(
                            PlayStates.SWITCHED,
                            eventTime.eventPlaybackPositionMs,
                            bitrateKbps
                        )
                    }
                }
            }
        // if there are text tracks, but none are selected, use first text track to signal inactive
        if (textTrackFormat != null && !activeTextTrackFound)
            einbliqIoSession.addCustomData(textTrackFormat!!.toTextTrackCustomData(false))
    }

    override fun onAudioUnderrun(
        eventTime: AnalyticsListener.EventTime,
        bufferSize: Int,
        bufferSizeMs: Long,
        elapsedSinceLastFeedMs: Long
    ) {
        einbliqIoSession.addError(
            "Audio underrun",
            ErrorSeverities.ERROR,
            "bufferSize=$bufferSize ms elapsed=$elapsedSinceLastFeedMs ms"
        )
    }

    override fun onAudioCodecError(
        eventTime: AnalyticsListener.EventTime,
        audioCodecError: java.lang.Exception
    ) {
        einbliqIoSession.addError(
            audioCodecError.message ?: "unknown exoplayer audio codec error",
            ErrorSeverities.WARNING,
            audioCodecError.cause?.message
        )
    }

    override fun onVideoCodecError(
        eventTime: AnalyticsListener.EventTime,
        videoCodecError: java.lang.Exception
    ) {
        einbliqIoSession.addError(
            videoCodecError.message ?: "unknown exoplayer video codec error",
            ErrorSeverities.WARNING,
            videoCodecError.cause?.message
        )
    }

    override fun onAudioDecoderInitialized(
        eventTime: AnalyticsListener.EventTime,
        decoderName: String,
        initializedTimestampMs: Long,
        initializationDurationMs: Long
    ) {
        einbliqIoSession.addCustomData(
            mapOf(
                "android.decoder.audio" to decoderName,
                "android.decoder.audio.initInMillis" to initializationDurationMs
            )
        )
    }

    override fun onVideoDecoderInitialized(
        eventTime: AnalyticsListener.EventTime,
        decoderName: String,
        initializedTimestampMs: Long,
        initializationDurationMs: Long
    ) {
        einbliqIoSession.addCustomData(
            mapOf(
                "android.decoder.video" to decoderName,
                "android.decoder.video.initInMillis" to initializationDurationMs
            )
        )
    }

    override fun onAudioSinkError(
        eventTime: AnalyticsListener.EventTime,
        audioSinkError: Exception
    ) {
        einbliqIoSession.addError(
            "AudioSinkError",
            ErrorSeverities.ERROR,
            audioSinkError.message
        )
    }

    override fun onDroppedVideoFrames(
        eventTime: AnalyticsListener.EventTime,
        droppedFrames: Int,
        elapsedMs: Long
    ) {
        einbliqIoSession.addDroppedFrames(droppedFrames)
    }

    override fun onSurfaceSizeChanged(
        eventTime: AnalyticsListener.EventTime,
        width: Int,
        height: Int
    ) {
        einbliqIoSession.addCustomData(hashMapOf("android.surfaceSize" to "${width}x${height}"))
    }

    override fun onDrmSessionManagerError(
        eventTime: AnalyticsListener.EventTime,
        error: java.lang.Exception
    ) {
        einbliqIoSession.addError(
            error.message ?: "unspecified exoplayer drm error",
            ErrorSeverities.ERROR,
            error.cause?.message
        )
    }
}

private fun MediaItem.DrmConfiguration.toDrmModel() = DrmModel(scheme.toString(), licenseUri.toString())

private fun Format.toTextTrackCustomData(active: Boolean) = hashMapOf(
    "android.subtitleTrack" to hashMapOf(
        "lang" to language,
        "active" to active
    )
)

private fun Format.toAudioTrackCustomData() = hashMapOf(
    "android.audioTrack" to hashMapOf(
        "lang" to language,
        "label" to label
    )
)

private fun Format.getPrimaryTrackType(): Int {
    val trackType = MimeTypes.getTrackType(sampleMimeType)
    return when {
        trackType != C.TRACK_TYPE_UNKNOWN -> trackType
        MimeTypes.getVideoMediaMimeType(codecs) != null -> C.TRACK_TYPE_VIDEO
        MimeTypes.getAudioMediaMimeType(codecs) != null -> C.TRACK_TYPE_AUDIO
        width != Format.NO_VALUE || height != Format.NO_VALUE -> C.TRACK_TYPE_VIDEO
        channelCount != Format.NO_VALUE || sampleRate != Format.NO_VALUE -> C.TRACK_TYPE_AUDIO
        else -> return C.TRACK_TYPE_UNKNOWN
    }
}
