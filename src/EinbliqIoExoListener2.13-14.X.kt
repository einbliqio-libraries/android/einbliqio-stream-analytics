package io.einbliq.streamanalytics.adapter

import com.google.android.exoplayer2.C
import com.google.android.exoplayer2.ExoPlaybackException
import com.google.android.exoplayer2.Format
import com.google.android.exoplayer2.PlaybackParameters
import com.google.android.exoplayer2.analytics.AnalyticsListener
import com.google.android.exoplayer2.source.TrackGroupArray
import com.google.android.exoplayer2.trackselection.BaseTrackSelection
import com.google.android.exoplayer2.trackselection.TrackSelectionArray
import com.google.android.exoplayer2.util.MimeTypes
import io.einbliq.streamanalytics.EinbliqIoExoPlayerMappings.mapExoIsPlayingToEinbliqPlayState
import io.einbliq.streamanalytics.EinbliqIoExoPlayerMappings.mapExoPlayStateToEinbliqPlayState
import io.einbliq.streamanalytics.EinbliqIoSession
import io.einbliq.streamanalytics.ErrorSeverities
import io.einbliq.streamanalytics.PlayStates

// EinbliqIoExoListener for ExoPlayer 2.13.x and 2.14.x
@Suppress("ClassName")
@Deprecated("compatible up to einbliq.io library version 1.1.0 only, please contact support if you need to continue using this exoplayer version with einbliq.io version 1.2.0+")
class EinbliqIoExoListener2_13(private val einbliqIoSession: EinbliqIoSession) : AnalyticsListener {

    override fun onPlaybackStateChanged(eventTime: AnalyticsListener.EventTime, state: Int) {
        val einbliqPlayState = mapExoPlayStateToEinbliqPlayState(state)
        einbliqIoSession.addPlayState(einbliqPlayState, eventTime.eventPlaybackPositionMs, null)
        if (einbliqPlayState == PlayStates.ENDED)
            einbliqIoSession.terminate()
    }

    override fun onIsPlayingChanged(eventTime: AnalyticsListener.EventTime, isPlaying: Boolean) {
        val state = mapExoIsPlayingToEinbliqPlayState(isPlaying)
        einbliqIoSession.addPlayState(state, eventTime.eventPlaybackPositionMs, null)
    }

    override fun onSeekStarted(eventTime: AnalyticsListener.EventTime) {
        einbliqIoSession.addPlayState(PlayStates.SEEKING, eventTime.eventPlaybackPositionMs, null)
    }

    override fun onPlaybackParametersChanged(eventTime: AnalyticsListener.EventTime, playbackParameters: PlaybackParameters) {
        einbliqIoSession.addCustomData(hashMapOf(
                "android.playbackSpeed" to playbackParameters.speed
        ))
    }

    override fun onPlayerError(eventTime: AnalyticsListener.EventTime, error: ExoPlaybackException) {
        einbliqIoSession.addError(
                error.message ?: "unknown exoplayer error",
                ErrorSeverities.ERROR,
                error.cause?.message)
    }

    override fun onTracksChanged(eventTime: AnalyticsListener.EventTime, trackGroups: TrackGroupArray, trackSelections: TrackSelectionArray) {
        // this is for switching audio track, subtitle track or video quality
        var hasActiveSubtitleTrack = false
        for (i in 0 until trackSelections.length) {
            val selection = trackSelections.get(i) as? BaseTrackSelection ?: continue
            val format = selection.selectedFormat
            when (format.getPrimaryTrackType()) {
                C.TRACK_TYPE_VIDEO -> {
                    val bitrateKbps = format.bitrate / 1000
                    einbliqIoSession.addPlayState(
                            PlayStates.SWITCHED,
                            eventTime.eventPlaybackPositionMs,
                            bitrateKbps)
                }
                C.TRACK_TYPE_AUDIO -> {
                    einbliqIoSession.addCustomData(format.toAudioTrackCustomData())
                }
                C.TRACK_TYPE_TEXT -> {
                    hasActiveSubtitleTrack = true
                    einbliqIoSession.addCustomData(format.toTextTrackCustomData(true))
                }
                else -> {} // ignore
            }
        }
        if (hasActiveSubtitleTrack) return
        for (i in 0 until trackGroups.length) {
            val group  = trackGroups[i]
            if (group.length == 0) continue
            val format = group.getFormat(0)
            if (format.getPrimaryTrackType() == C.TRACK_TYPE_TEXT) {
                einbliqIoSession.addCustomData(format.toTextTrackCustomData(false))
                break
            }
        }
    }

    override fun onAudioUnderrun(eventTime: AnalyticsListener.EventTime, bufferSize: Int, bufferSizeMs: Long, elapsedSinceLastFeedMs: Long) {
        einbliqIoSession.addError(
                "Audio underrun",
                ErrorSeverities.ERROR,
                "bufferSize=$bufferSize ms elapsed=$elapsedSinceLastFeedMs ms"
        )
    }

    override fun onAudioSinkError(eventTime: AnalyticsListener.EventTime, audioSinkError: Exception) {
        einbliqIoSession.addError(
                "AudioSinkError",
                ErrorSeverities.ERROR,
                audioSinkError.message
        )
    }

    override fun onDroppedVideoFrames(eventTime: AnalyticsListener.EventTime, droppedFrames: Int, elapsedMs: Long) {
        einbliqIoSession.addDroppedFrames(droppedFrames)
    }

    override fun onSurfaceSizeChanged(eventTime: AnalyticsListener.EventTime, width: Int, height: Int) {
        einbliqIoSession.addCustomData(hashMapOf("android.surfaceSize" to "${width}x${height}"))
    }

}

private fun Format.toTextTrackCustomData(active: Boolean) = hashMapOf(
        "android.subtitleTrack" to hashMapOf(
                "lang" to language,
                "active" to active
        )
)

private fun Format.toAudioTrackCustomData() = hashMapOf(
        "android.audioTrack" to hashMapOf(
                "lang" to language,
                "label" to label
        )
)

private fun Format.getPrimaryTrackType(): Int {
    val trackType = MimeTypes.getTrackType(sampleMimeType)
    return when {
        trackType != C.TRACK_TYPE_UNKNOWN -> trackType
        MimeTypes.getVideoMediaMimeType(codecs) != null -> C.TRACK_TYPE_VIDEO
        MimeTypes.getAudioMediaMimeType(codecs) != null -> C.TRACK_TYPE_AUDIO
        width != Format.NO_VALUE || height != Format.NO_VALUE -> C.TRACK_TYPE_VIDEO
        channelCount != Format.NO_VALUE || sampleRate != Format.NO_VALUE -> C.TRACK_TYPE_AUDIO
        else -> return C.TRACK_TYPE_UNKNOWN
    }
}
